﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

    public Transform Player1;
    public Transform Player2;
    private Transform target;
	private Vector2 heading = Vector2.right;
    // public parameters with default values
    public float minSpeed, maxSpeed;
    public float minTurnSpeed, maxTurnSpeed;
    // private state
    private float speed;
    private float turnSpeed;
    public ParticleSystem explosionPrefab;

    // Use this for initialization
    void Start () {
        // find a player object to be the target by type
        PlayerMove player = FindObjectOfType<PlayerMove>();
        target = player.transform;
        // bee initially moves in random direction
        heading = Vector2.right;
        float angle = Random.value * 360;
        heading = heading.Rotate(angle);
        // set speed and turnSpeed randomly
        speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
        turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed, Random.value);
    }
	


    // Update is called once per frame
    void Update () {

        Transform target;
        target = Player1;

        if(Vector2.Distance(Player1.position,transform.position) < Vector2.Distance(Player2.position, transform.position))
        {
            target = Player1;
        }
        else
        {
            target = Player2;
        }

		Vector2 direction = target.position - transform.position;


        // calculate how much to turn per frame
        float angle = turnSpeed * Time.deltaTime;
    // turn left or right
	if (direction.IsOnLeft(heading)) {
    // target on left, rotate anticlockwise
		heading = heading.Rotate(angle);
}
	else {
    // target on right, rotate clockwise
		heading = heading.Rotate(-angle);
}
	transform.Translate(heading * speed * Time.deltaTime);
	}
	void OnDrawGizmos() {
    // draw heading vector in red
	Gizmos.color = Color.red;
	Gizmos.DrawRay(transform.position, heading);
    // draw target vector in yellow
	Gizmos.color = Color.yellow;
	Vector2 direction = target.position - transform.position;
	Gizmos.DrawRay(transform.position, direction);
}
    void OnDestroy()
    {
        // create an explosion at the bee's current position
        ParticleSystem explosion = Instantiate(explosionPrefab);
        explosion.transform.position = transform.position;
        Destroy(explosion.gameObject, explosion.duration);
    }
}
