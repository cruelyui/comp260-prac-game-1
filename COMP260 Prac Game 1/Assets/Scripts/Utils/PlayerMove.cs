﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

    public string MoveHor;
    public string MoveVer;
    private BeeSpawner beeSpawner;
    public Vector2 velocity;
	public float maxSpeed = 5.0f;
    public float acceleration = 10.0f;
    public float brake = 5.0f;
    public float turnSpeed = 30.0f;
    public float destroyRadius = 1.0f;
    private float speed = 0.0f;
    
    void Start () {
        // find the bee spawner and store a reference for later
        beeSpawner = FindObjectOfType<BeeSpawner>();
    }
	

    void Update () {

        if (Input.GetButtonDown("Fire1"))
        {
            // destroy nearby bees
            beeSpawner.DestroyBees(
            transform.position, destroyRadius);
        }


        Vector2 direction;
        direction.x = Input.GetAxis(MoveHor);
        direction.y = Input.GetAxis(MoveVer);

        float turn = Input.GetAxis(MoveHor);

        // turn the car
        transform.Rotate(0, 0, -turn * turnSpeed * Time.deltaTime*speed);

        // the vertical axis controls acceleration fwd/back
        float forwards = Input.GetAxis(MoveVer);

        if (forwards > 0)
        {
            // accelerate forwards
            speed = speed + acceleration * Time.deltaTime;
        }
        else if (forwards < 0)
        {
            // accelerate backwards
            speed = speed - acceleration * Time.deltaTime;
        }
        else
        {
            // braking
            if (speed > 0)
            {
                speed = speed - brake * Time.deltaTime;
                if(speed < 0)
                {
                    speed = 0;
                }
            }
            else
            {
                speed = speed + brake * Time.deltaTime;
                if(speed > 0)
                {
                    speed = 0;
                }
            }
        }


        // clamp the speed
        speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

        // scale by the maxSpeed parameter
        Vector2 velocity = Vector2.up * speed;

        // move the object
        transform.Translate(velocity * Time.deltaTime, Space.Self);
	}
}
